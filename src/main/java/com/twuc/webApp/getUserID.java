package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

@RestController
public class getUserID {
    //2.1
    @GetMapping("/api/users/{userId}")
    String getUserID(@PathVariable Integer userId){
        return userId.toString();
    }

    @GetMapping("/api/users2/{userId}")
    int getUserIDbyInteger(@PathVariable("userId") int userID){
        return  userID;
    }

    @GetMapping("/api/users/book/")
    String getparameter(@RequestParam String name ){
        return  "this is "+name;
    }


    @GetMapping("/api/users/{userId}/books/{bookId}")
    String getRequest(@PathVariable int userId, @PathVariable int bookId ){
        return  "this is "+userId+" and "+bookId;
    }
    //2.3
    @GetMapping("/api/users/")
    String getJson() throws JsonProcessingException {
        ObjectMapper om=new ObjectMapper();
        String str=om.writeValueAsString(new Person("xiaoming"));
        return str;

    }



}
