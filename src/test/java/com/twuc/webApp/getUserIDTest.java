package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@AutoConfigureMockMvc
class getUserIDTest {
    @Autowired
    private MockMvc mockMvc;
    //2.1
    @Test
    public void should_return_right() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("2"));
    }
    @Test
    public void should_get_userid() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users2/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("3"));
    }
    @Test
    public void should_get_userid_bookid() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("this is 2 and 3"));
    }
    //2.2
    @Test
    public void should_get() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/book/?name=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("this is xiaoming"));
    }

    //2.3
    @Test
    public void should_get_json() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/"))
                .andExpect(MockMvcResultMatchers.content().string("{\"name\":\"xiaoming\"}"));
    }



}